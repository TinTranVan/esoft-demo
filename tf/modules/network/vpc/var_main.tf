### General

variable "project_prefix" {
  description = "Project Name"
}

variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "eu-west-1"
}

variable "region_prefix" {
  default = "euwt1"
}

#variable "aws_account_id" {
#  description = "AWS account ID"
#}

### Environmet

variable "env" {
  default = "prod"
}

### VPC var
variable "vpc_cidr" {
  default = "192.168.38.0/24"
}
variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

