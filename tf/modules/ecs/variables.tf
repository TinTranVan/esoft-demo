variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "eu-west-1"
}

#variable "aws_account_id" {
#  description = "AWS account ID"
#}

# Network
variable "vpc_cidr" {
  default = "192.168.43.0/24"
}
variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

# Project Genral
variable "project_prefix" {
  default = "esoft-demo"
}

variable "region_prefix" {
  default = "euwt1"
}

variable "env" {
  default = "prod"
}

#variable "prefix_fullname" {
#  default = "${var.env}-${var.project_prefix}-${var.region_prefix}"
#}



variable "service_port" {
  default = 80
}




variable "app_count" {
  description = "Number of docker containers to run"
  default     = 1
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = 256
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = 512
}

# DB Variable

variable "db_name" {
  default = "wordpress"
}

variable "db_user" {
  default =  "wordpress"
  
}

variable "db_password" {
}

variable "db_instance_type" {
  default = "db.t2.micro"
}

variable "mysql_engine" {
  default = "MySQL"
}

variable "mysql_version" {
  default = "5.7.34"
}

# Envinronment df

variable "wordpress_image" {
  default =  "bitnami/wordpress:5.8.2"
}

variable "env_environment" {
  default = "PROD"
}

