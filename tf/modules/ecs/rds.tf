

resource "aws_db_subnet_group" "default" {
    name        = "${local.vars.prefix_fullname}-001-dbsubnet"
    description = "VPC Subnets"
    subnet_ids  = aws_subnet.private.*.id
}

resource "aws_db_instance" "wordpress" {
    identifier             = "wordpress-tf"
    allocated_storage      = 5
    engine                 = var.mysql_engine
    engine_version         = var.mysql_version
    port                   = "3306"
    instance_class         = "${var.db_instance_type}"
    name                   = "${var.db_name}"
    username               = "${var.db_user}"
    password               = "${var.db_password}"
    availability_zone      = "${var.aws_region}b"
    vpc_security_group_ids = ["${aws_security_group.db.id}"]
    multi_az               = false
    db_subnet_group_name   = "${aws_db_subnet_group.default.id}"
    parameter_group_name   = "default.mysql5.7"
    publicly_accessible    = false
}