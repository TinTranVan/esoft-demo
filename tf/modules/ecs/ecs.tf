### ECR
resource "aws_ecr_repository" "main" {
  name                 = "${var.env}-${var.project_prefix}-ecr"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_ecr_lifecycle_policy" "main_ecr_policy" {
  repository = aws_ecr_repository.main.name
  policy = <<EOF
{
  "rules": [
    {
      "rulePriority": 1,
      "description": "Keep latest 5 images",
      "selection": {
        "tagStatus": "any",
        "countType": "imageCountMoreThan",
        "countNumber": 5
      },
      "action": {
        "type": "expire"
      }
    }
  ]
}
  EOF 
}
### ECS

data "template_file" "task-def" {
  template = "${file("${path.module}/template/task-def.json")}"
  vars = {
    env_environment = var.env
    app_image = var.wordpress_image
    prefix_fullname = local.vars.prefix_fullname
    env_db_host = aws_db_instance.wordpress.endpoint
    env_db_name = var.db_name
    env_db_user = var.db_user
    env_db_password = var.db_password
    service_port = var.service_port
    aws_region = var.aws_region

  }
 depends_on = [
    "aws_db_instance.wordpress"
  ]

}


resource "aws_ecs_cluster" "main" {
  name = "${local.vars.prefix_fullname}-001-ecs"
}

#data "aws_iam_role" "ecs_task_execution_role" {
#  name = "ecsTaskExecutionRole"
#}

resource "aws_iam_role" "ecs_role" {
  name = "ecsTaskExecutionRoleTerraform"
  assume_role_policy = jsonencode ({
    "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
  })
}

resource "aws_iam_role_policy_attachment" "ecs_role_attach" {
  role = aws_iam_role.ecs_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_cloudwatch_log_group" "ecs_log_group" {
  name = "/ecs/${local.vars.prefix_fullname}-001-df"
}

resource "aws_ecs_task_definition" "service" {
  family                   = "${local.vars.prefix_fullname}-001-df"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  #execution_role_arn = "${data.aws_iam_role.ecs_task_execution_role.arn}"
  execution_role_arn = aws_iam_role.ecs_role.arn
  cpu	= "${var.fargate_cpu}"
  memory = "${var.fargate_memory}"
  
  container_definitions = "${data.template_file.task-def.rendered}"
  depends_on = [
    "aws_iam_role_policy_attachment.ecs_role_attach",
    "aws_cloudwatch_log_group.ecs_log_group"
  ]
}

resource "aws_ecs_service" "main" {
  name            = "${local.vars.prefix_fullname}-001-service"
  cluster         = "${aws_ecs_cluster.main.id}"
  task_definition = "${aws_ecs_task_definition.service.arn}"
  desired_count   = "${var.app_count}"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = ["${aws_security_group.ecs_tasks.id}"]
    subnets         = aws_subnet.private.*.id
    assign_public_ip = true
  }

    
  load_balancer {  
      target_group_arn = "${aws_alb_target_group.service.id}"
      container_name   = "${local.vars.prefix_fullname}-001-container"
      container_port   = "${var.service_port}"
    }

  depends_on = [
    "aws_alb_listener.service"
  ]
}
