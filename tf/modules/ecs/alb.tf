### ALB

resource "aws_alb" "main" {
  name            = "${local.vars.prefix_fullname}-001-alb"
  subnets         = aws_subnet.public.*.id
  security_groups = ["${aws_security_group.lb.id}"]
}

resource "aws_alb_target_group" "service" {
  name        = "${local.vars.prefix_fullname}-001-target"
  port        = "8080"
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.main.id}"
  target_type = "ip"
}


# Redirect all traffic from the ALB to the target group

resource "aws_alb_listener" "service" {
  load_balancer_arn = "${aws_alb.main.id}"
  port              = "${var.service_port}"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.service.id}"
    type             = "forward"
  }
}