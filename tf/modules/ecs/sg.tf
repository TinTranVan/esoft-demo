### Security

# ALB Security group
# This is the group you need to edit if you want to restrict access to your application
resource "aws_security_group" "lb" {
  name        = "${local.vars.prefix_fullname}-alb-sg"
  description = "controls access to the ALB"
  vpc_id      = "${aws_vpc.main.id}"



  ingress {
    protocol    = "tcp"
    from_port   = "${var.service_port}"
    to_port     = "${var.service_port}"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${local.vars.prefix_fullname}-001-alb-sg"
  }

}

# Traffic to the ECS Cluster should only come from the ALB
resource "aws_security_group" "ecs_tasks" {
  name        = "${local.vars.prefix_fullname}-sg"
  description = "allow inbound access from the ALB only"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    protocol        = "tcp"
    from_port       = "${var.service_port}"
    to_port         = "${var.service_port}"
    security_groups = ["${aws_security_group.lb.id}"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${local.vars.prefix_fullname}-001-sg"
  }

}


resource "aws_security_group" "db" {
  name        = "${local.vars.prefix_fullname}-db-sg"
  description = "allow inbound access from the ECS only"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    protocol        = "tcp"
    from_port       = "3306"
    to_port         = "3306"
    security_groups = ["${aws_security_group.ecs_tasks.id}"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${local.vars.prefix_fullname}-001-db1-sg"
  }

}