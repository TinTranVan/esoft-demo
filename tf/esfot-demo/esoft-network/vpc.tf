module "esoft-vpc" {
    source = "../../modules/network/vpc"
    project_prefix = "esoft-demo"
    aws_region = var.aws_region
    vpc_cidr = var.vpc_cidr
    az_count = var.az_count
}