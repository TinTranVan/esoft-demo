### General ###
variable "env" {
  default = "prod"
}
### VPC Var ###
variable "aws_region" {
    default = "eu-west-1"
}

variable "region_prefix" {
  default = "euwt1"
}

variable "vpc_cidr" {
  default = "192.168.38.0/24"
}
variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}
