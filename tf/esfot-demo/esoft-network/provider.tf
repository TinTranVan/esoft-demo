provider "aws" {

  region     = "${var.aws_region}"
  shared_credentials_file = "/root/.aws/credentials"
  profile = "default"
}

### Network

# Fetch AZs in the current region
data "aws_availability_zones" "available" {}



