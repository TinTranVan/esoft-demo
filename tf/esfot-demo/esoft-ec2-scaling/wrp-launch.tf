resource "template_file" "cloud-userdata" {
    template = "${file("user-data.tpl")}"
}

resource "aws_launch_configuration" "web" {
  name_prefix = "${local.vars.prefix_fullname}-ec2-"

  image_id = "ami-059236d06b055b2cf"
  instance_type = "t2.micro"
  key_name = "ttranvan"

  security_groups = [aws_security_group.ec2_sg.id]
  associate_public_ip_address = true

user_data = "${template_file.cloud-userdata.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}