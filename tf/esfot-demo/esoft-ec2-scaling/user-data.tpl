#cloud-config
package_update: true
packages:
  - php
  - php-fpm
  - nginx
  - ruby-full
  - wget
runcmd:
  - apt-get remove apache2 -y
  - [ systemctl, enable, nginx ]
  - [ systemctl, start, nginx ]
runcmd:
    - wget https://aws-codedeploy-eu-west-1.s3.eu-west-1.amazonaws.com/latest/install -O /tmp/install
    - chmod +x /tmp/install
    - /tmp/install auto > /tmp/logfile
    - service codedeploy-agent start
runcmd:
    - echo "Welcome to ESOFT-SITE-AUTOSCALE" > /var/www/html/index.html