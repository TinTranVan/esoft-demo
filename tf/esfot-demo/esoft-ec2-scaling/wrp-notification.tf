resource "aws_sns_topic" "web" {
  name = "${local.vars.prefix_fullname}-sns-topic"
}

resource "aws_sns_topic_subscription" "web" {
  topic_arn = aws_sns_topic.web.arn
  protocol  = "email"
  endpoint  = var.email_notification
}

resource "aws_autoscaling_notification" "web" {
  group_names = [
    aws_autoscaling_group.web.name
  ]

  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
    "autoscaling:EC2_INSTANCE_TERMINATE_ERROR",
  ]

  topic_arn = aws_sns_topic.web.arn
}