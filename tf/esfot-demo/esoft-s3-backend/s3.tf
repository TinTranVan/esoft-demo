provider "aws" {
  region = "eu-west-1"
}
# terraform state file setup
# create an S3 bucket to store the state file in
resource "aws_s3_bucket" "terraform-state-storage-s3" {
    bucket = "esoft-tf-state-s3"
 
    versioning {
      enabled = true
    }
 
    lifecycle {
      prevent_destroy = true
    }
 
    tags = {
    Name        = "S3 Remote Terraform State Store"
    }      
}