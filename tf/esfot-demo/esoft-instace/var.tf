### General ###
variable "project_prefix" {
  default = "esoft-demo"
} 

variable "env" {
  default = "prod"
}
### VPC Var ###
variable "aws_region" {
    default = "eu-west-1"
}

variable "region_prefix" {
  default = "euwt1"
}

#variable "aws_account_id" {
#  description = "AWS account ID"
#}

### Environmet



variable "http_port" {
    default = 80
}

variable "https_port" {
    default = 443
}

variable "private_ip" {
    default = "1.53.10.129/32"
}

variable "vpc_id" {
    default = "vpc-0590cf2aa9b21cb35"
}

variable "private_subnet" {
    default = ""
}

variable "public_subnet" {
    default = "	subnet-05401e5a554199983"
}