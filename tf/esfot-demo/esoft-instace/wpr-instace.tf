
module "wpr_instance" {
    source                 = "../../modules/instace"
    name                   = "${local.vars.prefix_fullname}-ec2-001"
    instance_count         = 1
    ami                    = "ami-059236d06b055b2cf"
    instance_type          = "t2.small"
    key_name               = "ttranvan"
    monitoring             = true
    vpc_security_group_ids = [aws_security_group.sg_1.id]
    subnet_id              = var.public_subnet
    associate_public_ip_address = true
#    user_data = "${data.template_cloudinit_config.config.rendered}"
    tags = {
      Terraform = "true"
      Environment = var.env
  }
}

