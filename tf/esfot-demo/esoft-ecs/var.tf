### General ###
variable "project_prefix" {
  default = "esoft-demo"
} 

variable "env" {
  default = "prod"
}
### VPC Var ###
variable "aws_region" {
    default = "eu-west-1"
}

variable "region_prefix" {
  default = "euwt1"
}

variable vpc_cidr {
  default = "192.168.43.0/24"
} 