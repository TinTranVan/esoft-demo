
module "wpr_ecs" {
    source  = "../../modules/ecs"
    project_prefix = "esoft-wpr"
    region_prefix = "euwt1"
    env = var.env
    vpc_cidr = var.vpc_cidr
    service_port = 80
    fargate_cpu = 256
    fargate_memory = 512
    db_password = "pass#word12"
    mysql_version = "5.7.34"

#    user_data = "${data.template_cloudinit_config.config.rendered}"

}

